package dk.semlerit.autocore.web.exception;

import java.util.Objects;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
abstract class NestedExceptionUtils {
	/**
	 * Build a message for the given base message and root cause.
	 * 
	 * @param message
	 *            the base message
	 * @param cause
	 *            the root cause
	 * @return the full exception message
	 */
	public static String buildMessage(String message, Throwable cause) {
		if (Objects.isNull(cause)) {
			return message;
		}
		StringBuilder sb = new StringBuilder(64);
		if (Objects.nonNull(message)) {
			sb.append(message).append("; ");
		}
		sb.append("nested exception is ").append(cause);
		return sb.toString();
	}

	/**
	 * Retrieve the innermost cause of the given exception, if any.
	 * 
	 * @param original
	 *            the original exception to introspect
	 * @return the innermost exception, or {@code null} if none
	 */
	public static Throwable getRootCause(Throwable original) {
		if (Objects.isNull(original)) {
			return null;
		}
		Throwable rootCause = null;
		Throwable cause = original.getCause();
		while (Objects.nonNull(cause) && Objects.nonNull(cause)) {
			rootCause = cause;
			cause = cause.getCause();
		}
		return rootCause;
	}

	/**
	 * Retrieve the most specific cause of the given exception, that is, either
	 * the innermost cause (root cause) or the exception itself.
	 * <p>
	 * Differs from {@link #getRootCause} in that it falls back to the original
	 * exception if there is no root cause.
	 * 
	 * @param original
	 *            the original exception to introspect
	 * @return the most specific cause (never {@code null})
	 * @since 4.3.9
	 */
	public static Throwable getMostSpecificCause(Throwable original) {
		Throwable rootCause = getRootCause(original);
		return (Objects.nonNull(rootCause) ? rootCause : original);
	}
}
