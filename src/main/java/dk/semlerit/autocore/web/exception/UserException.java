package dk.semlerit.autocore.web.exception;

/**
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class UserException extends NestedRuntimeException {
	private static final long serialVersionUID = 4964360665580271214L;

	public UserException(String msg) {
		super(msg);
	}

	public UserException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public UserException(String message, Object... objects) {
		super(message, objects);
	}

	public UserException(String message, Throwable cause, Object... objects) {
		super(message, cause, objects);
	}
}
