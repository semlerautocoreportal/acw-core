package dk.semlerit.autocore.web.core.context;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.portlet.PortletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ibm.portal.um.PumaHome;
import com.ibm.portal.um.PumaProfile;
import com.ibm.portal.um.User;
import com.ibm.portal.um.exceptions.PumaException;
import com.ibm.websphere.cache.DistributedMap;
import com.ibm.websphere.cache.EntryInfo;
import com.ibm.ws.portletcontainer.portlet.PortletUtils;

import dk.semlerit.autocore.web.exception.UserException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
@Slf4j
@ApplicationScoped
public class UserContextAccessor {

	/**
	 * Convenience enum of allowed values, should be updated as necessary with new values
	 */
	private static enum ContextKeys {
		LAST_PAGE_VISITED, CURRENT_DEALER_NUMBER, PREVIOUS_DEALER_NUMBER, WINDOW_ID_RESET_AFTER_DEALER_NUMBER_CHANGE;
	}

	private static final String CLASS_NAME = UserContextAccessor.class.getName();
	private static final String PACKAGE_NAME = UserContextAccessor.class.getPackage().getName();
	private static final String CACHEKEY_USER_DN = "userDNKey";
	private static final String CACHEKEY_USER_LOGGED_IN_CHECK = "userLoggedInCheckKey";

	@Resource(lookup = CoreConstants.USER_CONTEXT_JNDINAME)
	private DistributedMap distributedUserContextMap;
	@Resource(lookup = PumaHome.JNDI_NAME)
	private PumaHome pumaHome;

	/**
	 * Check whether selected dealernumber has recently changed.
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return {@code true} if dealernumber has changed, otherwise {@code false}
	 */
	public boolean hasDealerNumberChanged(PortletRequest request, String sessionAlias) {
		log.debug("ENTRY - sessionAlias = {}", sessionAlias);

		String currentlySelectedDealerNumber = getCurrentDealerNumber(request, sessionAlias);
		String previouslySelectedDealerNumber = getPreviousDealerNumber(request, sessionAlias);

		if (Objects.nonNull(previouslySelectedDealerNumber) && Objects.nonNull(currentlySelectedDealerNumber)
				&& !Objects.equals(previouslySelectedDealerNumber, currentlySelectedDealerNumber)) {
			Set<String> windowIds = getWindowIDResetAfterDealerNumberChange(request, sessionAlias);
			if (windowIds.stream().anyMatch(windowId -> windowId.equals(request.getWindowID()))) {
				log.debug(
						"Return false, for UserDN() = {} has currentlySelectedDealerNumber {} and previouslySelectedDealerNumber {}",
						getUserDN(request.getRemoteUser()), currentlySelectedDealerNumber,
						previouslySelectedDealerNumber);
				return false;
			}
			log.debug(
					"Return true, for UserDN() = {} has currentlySelectedDealerNumber {} and previouslySelectedDealerNumber {}",
					getUserDN(request.getRemoteUser()), currentlySelectedDealerNumber, previouslySelectedDealerNumber);
			return true;
		}
		log.debug(
				"Return false, for UserDN() = {} has currentlySelectedDealerNumber {} and previouslySelectedDealerNumber {}",
				getUserDN(request.getRemoteUser()), currentlySelectedDealerNumber, previouslySelectedDealerNumber);
		return false;
	}

	/**
	 * Retrieve the currently selected dealer number from the global user context. Selection will account for multiple
	 * sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @return dealer number or {@literal null} if no dealer number has been selected
	 */
	public String getCurrentDealerNumber(PortletRequest request) {
		return getCurrentDealerNumber(request, null);
	}

	/**
	 * Retrieve the currently selected dealer number from the global user context. Selection will account for multiple
	 * sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @return dealer number or {@literal null} if no dealer number has been selected
	 */
	public String getCurrentDealerNumber(PortletRequest request, String sessionAlias) {
		return getCurrentDealerNumber(getHttpServletRequest(request), sessionAlias);
	}

	/**
	 * Retrieve the currently selected dealer number from the global user context. Selection will account for multiple
	 * sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @return dealer number or {@literal null} if no dealer number has been selected
	 */
	public String getCurrentDealerNumber(HttpServletRequest request) {
		return getCurrentDealerNumber(request, null);
	}

	/**
	 * Retrieve the currently selected dealer number from the global user context. Selection will account for multiple
	 * sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @return dealer number or {@literal null} if no dealer number has been selected
	 */
	public String getCurrentDealerNumber(HttpServletRequest request, String sessionAlias) {
		log.debug("ENTRY - sessionAlias = {}", sessionAlias);

		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			Object dealerNumberObj = getUserContext(userContextKey).get(ContextKeys.CURRENT_DEALER_NUMBER.name());
			log.debug("dealerNumberObj() = {}", dealerNumberObj);
			if (Objects.isNull(dealerNumberObj)) {
				return null;
			}
			return String.valueOf(dealerNumberObj);
		} catch (IOException e) {
			log.error("Could not retrieve current dealer number for sessionAlias {}", sessionAlias, e);
			return null;
		}
	}

	/**
	 * Retrieve the previously currently selected dealer number from the global user context. Selection will account for
	 * multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @return dealer number or {@literal null} if no previous dealer number has been selected
	 */
	public String getPreviousDealerNumber(PortletRequest request, String sessionAlias) {
		return getPreviousDealerNumber(getHttpServletRequest(request), sessionAlias);
	}

	/**
	 * Retrieve the previously selected dealer number from the global user context. Selection will account for multiple
	 * sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @return dealer number or {@literal null} if no previous dealer number has been selected
	 */
	public String getPreviousDealerNumber(HttpServletRequest request, String sessionAlias) {
		log.debug("ENTRY - sessionAlias = {}", sessionAlias);

		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			Object previousDealerNumberObj = getUserContext(userContextKey)
					.get(ContextKeys.PREVIOUS_DEALER_NUMBER.name());
			if (Objects.isNull(previousDealerNumberObj)) {
				log.debug("No previous dealer number");
				return null;
			}

			log.debug("PreviousDealerNumber == {}", previousDealerNumberObj);
			return String.valueOf(previousDealerNumberObj);
		} catch (IOException e) {
			log.error("Could not retrieve current dealer number for sessionAlias {}", sessionAlias, e);
			return null;
		}
	}

	/**
	 * Set the currently selected dealer number. Old selection will be placed in previous selected dealerNumber.
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @param dealerNumber
	 */
	public void setCurrentDealerNumber(PortletRequest request, String sessionAlias, String dealerNumber) {
		setCurrentDealerNumber(getHttpServletRequest(request), sessionAlias, dealerNumber);
	}

	/**
	 * Set the currently selected dealer number. Old selection will be placed in previous selected dealerNumber.
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @param dealerNumber
	 */
	public void setCurrentDealerNumber(HttpServletRequest request, String sessionAlias, String dealerNumber) {
		log.debug("ENTRY - sessionAlias = {}, dealerNumber = {}", sessionAlias, dealerNumber);

		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			final Map<String, Object> userContextMap = getUserContext(userContextKey);
			userContextMap.put(ContextKeys.PREVIOUS_DEALER_NUMBER.name(),
					userContextMap.get(ContextKeys.CURRENT_DEALER_NUMBER.name()));
			userContextMap.put(ContextKeys.CURRENT_DEALER_NUMBER.name(), dealerNumber);
			userContextMap.remove(ContextKeys.WINDOW_ID_RESET_AFTER_DEALER_NUMBER_CHANGE.name());

			setUserContext(userContextKey, userContextMap);

			log.debug("sets dealerNumber {} for userContextKey {}", dealerNumber, userContextKey);

		} catch (IOException e) {
			log.error("Could not set current dealer number for sessionAlias {}", sessionAlias, e);
		}
	}

	/**
	 * Retrieve the last page visited.. Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return last page visited or {@literal null} if no registrations has been made
	 */
	public String getLastPageVisited(PortletRequest request, String sessionAlias) {
		return getLastPageVisited(getHttpServletRequest(request), sessionAlias);
	}

	/**
	 * Retrieve the last page visited..<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return last page visited or {@literal null} if no registrations has been made
	 */
	public String getLastPageVisited(HttpServletRequest request, String sessionAlias) {
		log.debug("ENTRY - sessionAlias = {}", sessionAlias);
		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			Object lastPageVisitedObj = getUserContext(userContextKey).get(ContextKeys.LAST_PAGE_VISITED.name());
			if (Objects.isNull(lastPageVisitedObj)) {
				return null;
			}

			return String.valueOf(lastPageVisitedObj);
		} catch (IOException e) {
			log.error("Could not retrieve last page visited sessionAlias {}", sessionAlias, e);
			return null;
		}
	}

	/**
	 * Set the last page visited<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @param uniquePageName
	 */
	public void setLastPageVisited(PortletRequest request, String sessionAlias, String uniquePageName) {
		setLastPageVisited(getHttpServletRequest(request), sessionAlias, uniquePageName);
	}

	/**
	 * Set the last page visited<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @param uniquePageName
	 */
	public void setLastPageVisited(HttpServletRequest request, String sessionAlias, String uniquePageName) {
		log.debug("ENTRY - sessionAlias = {}, uniquePageName = {}", sessionAlias, uniquePageName);
		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			final Map<String, Object> userContextMap = getUserContext(userContextKey);
			userContextMap.put(ContextKeys.LAST_PAGE_VISITED.name(), uniquePageName);
			setUserContext(userContextKey, userContextMap);
		} catch (IOException e) {
			log.error("Could not set last page visisted for sessionAlias {} with uniquePageName {}", sessionAlias,
					uniquePageName, e);
		}
	}

	/**
	 * Retrieve the window reset list which tracks the reset rules after a dealer number change.<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return
	 */
	public Set<String> getWindowIDResetAfterDealerNumberChange(PortletRequest request, String sessionAlias) {
		return getWindowIDResetAfterDealerNumberChange(getHttpServletRequest(request), sessionAlias);
	}

	/**
	 * Retrieve the window reset list which tracks the reset rules after a dealer number change.<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getWindowIDResetAfterDealerNumberChange(HttpServletRequest request, String sessionAlias) {
		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			return (Set<String>) Optional
					.ofNullable(getUserContext(userContextKey)
							.get(ContextKeys.WINDOW_ID_RESET_AFTER_DEALER_NUMBER_CHANGE.name()))
					.orElse(new HashSet<String>());
		} catch (IOException e) {
			log.error("Could not retrieve windowIDResetAfterDealerNumberChange for sessionAlias {}", sessionAlias, e);
		}
		return new HashSet<String>();
	}

	/**
	 * Mark a window as reset after a dealer number change.<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @param windowID
	 */
	public void markWindowAsReset(PortletRequest request, String sessionAlias, String windowID) {
		markWindowAsReset(getHttpServletRequest(request), sessionAlias, windowID);
	}

	/**
	 * Mark a window as reset after a dealer number change.<br/>
	 * Selection will account for multiple sessions. (Not yet implemented)
	 * 
	 * @param request
	 * @param sessionAlias
	 * @param windowID
	 */
	public void markWindowAsReset(HttpServletRequest request, String sessionAlias, String windowID) {
		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		try {
			final String userContextKey = resolveUserContextKey(cookies, getUserDN(request.getRemoteUser()),
					sessionAlias, isLoggedIn(request.getRemoteUser()));

			@SuppressWarnings("unchecked")
			final Map<String, Object> userContextMap = (Map<String, Object>) distributedUserContextMap
					.getOrDefault(userContextKey, new HashMap<String, Object>());

			@SuppressWarnings("unchecked")
			final Set<String> windowIDs = (Set<String>) userContextMap
					.getOrDefault(ContextKeys.WINDOW_ID_RESET_AFTER_DEALER_NUMBER_CHANGE.name(), new HashSet<String>());
			windowIDs.add(windowID);
			userContextMap.put(ContextKeys.WINDOW_ID_RESET_AFTER_DEALER_NUMBER_CHANGE.name(), windowIDs);
			setUserContext(userContextKey, userContextMap);
		} catch (IOException e) {
			log.error("Could not mark windowsAsReset for sessionAlias {} and windowsID {}", sessionAlias, windowID, e);
		}
	}

	/**
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return
	 * @throws IOException
	 */
	public Cookie getUserContextCookie(PortletRequest request, String sessionAlias) throws IOException {
		return getUserContextCookie(getHttpServletRequest(request), sessionAlias);
	}

	/**
	 * 
	 * @param request
	 * @param sessionAlias
	 * @return
	 * @throws IOException
	 */
	public Cookie getUserContextCookie(HttpServletRequest request, String sessionAlias) throws IOException {
		log.debug("ENTRY - sessionAlias = {}", sessionAlias);

		final ObjectMapper objectMapper = new ObjectMapper();
		ArrayNode cookieValues = objectMapper.createArrayNode();

		final Cookie[] cookies = Optional.ofNullable(request.getCookies()).orElseGet(() -> new Cookie[0]);
		log.debug("sessionAlias {}", sessionAlias);
		log.debug("cookies.length {}", cookies.length);
		for (final Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase(CoreConstants.COOKIE_NAME)) {
				// We got the cookie set
				String cookieValueStr = cookie.getValue();
				if (isBase64(cookieValueStr)) {
					cookieValueStr = new String(Base64.getDecoder().decode(cookieValueStr));
				}

				cookieValues = (ArrayNode) objectMapper.readTree(cookieValueStr);
				break;
			}
		}
		final String userDN = getUserDN(request.getRemoteUser());

		// Add additional temporary keys to Cookie
		log.debug("Starts logging additional temporary keys to cookie for user {}", userDN);
		Set<?> keys = distributedUserContextMap.keySet();
		for (final Object obj : keys) {
			final String key = (String) obj;
			if (key != null && key.startsWith(CoreConstants.USER_CONTEXT_PREFIX_TEMP)) {
				// This needs to have a sessionAlias prefixed that is not inside a LDAP DN (distinguished name).
				// Currently key is the DN of a user.
				// final String userDNToTest = substringBetween(key, CoreConstants.USER_CONTEXT_PREFIX_TEMP,
				// prefixSessionAlias(sessionAlias));
				final String userDNToTest = findUserDnFromTempKey(key, CoreConstants.USER_CONTEXT_PREFIX_TEMP,
						prefixSessionAlias(sessionAlias));
				if (!userDN.equals(userDNToTest)) {
					continue;
				}

				final String value = (String) distributedUserContextMap.get(key);

				ArrayNode sessionsContent = objectMapper.createArrayNode();
				ObjectNode session = objectMapper.createObjectNode();
				session.put(Optional.ofNullable(sessionAlias).orElseGet(() -> CoreConstants.DEFAULT_SESSION_ALIAS),
						value);
				sessionsContent.add(session);

				ObjectNode userNode = objectMapper.createObjectNode();
				userNode.put("userDN", userDN);
				userNode.set("sessions", sessionsContent);
				cookieValues.add(userNode);
			}
		}

		if (cookieValues.size() == 0) {
			log.debug("CookieValues are empty, returns null", userDN);
			return null;
		}

		final String encodedValue = Base64.getEncoder().encodeToString(cookieValues.toString().getBytes());
		final Cookie cookie = new Cookie(CoreConstants.COOKIE_NAME, encodedValue);
		cookie.setPath("/wps");
		cookie.setHttpOnly(true);
		cookie.setMaxAge(-1);
		cookie.setComment(String.format("New ACSAC cookie at %d", System.currentTimeMillis()));
		return cookie;
	}

	/**
	 * 
	 * @param distributedUserContextMap
	 */
	public void setUserContextMap(@NonNull DistributedMap distributedUserContextMap) {
		this.distributedUserContextMap = distributedUserContextMap;
	}

	/**
	 * 
	 * @param pumaHome
	 */
	public void setPumaHome(@NonNull PumaHome pumaHome) {
		this.pumaHome = pumaHome;
	}

	@PreDestroy
	public void preDestroy() {
		this.pumaHome = null;
		this.distributedUserContextMap = null;
	}

	// ~ Private internal methods =============================================

	@SuppressWarnings("unchecked")
	private Map<String, Object> getUserContext(String userContextKey) {
		return (Map<String, Object>) distributedUserContextMap.getOrDefault(userContextKey,
				new HashMap<String, Object>());
	}

	private void setUserContext(String userContextKey, Map<String, Object> userContext) {
		Objects.requireNonNull(userContextKey, "UserContextKey must not be null");
		Objects.requireNonNull(userContext, "UserContext must not be null");

		distributedUserContextMap.put(userContextKey, userContext, 1, -1, EntryInfo.SHARED_PUSH_PULL,
				new String[] { "acp.usercontext.id" });
	}

	private String getUserDN(final String remoteUser) {
		try {
			final String cachekey = String.join(".", PACKAGE_NAME, CLASS_NAME, CACHEKEY_USER_DN, remoteUser);
			final Object obj = distributedUserContextMap.get(cachekey);

			String userDN = (obj == null ? null : (String) obj);
			if (Objects.isNull(userDN)) {
				final PumaProfile pumaProfile = pumaHome.getProfile();
				final User user = pumaProfile.getCurrentUser();
				userDN = pumaProfile.getIdentifier(user);
				distributedUserContextMap.put(cachekey, userDN, 16, -1, 28800, EntryInfo.SHARED_PUSH_PULL,
						new Object[] { userDN });
			}
			return userDN;

		} catch (PumaException e) {
			log.error("Cannot find Distinguished Name for current user {}", remoteUser, e);

			throw new UserException("Cannot find Distinguished Name for current user");
		}
	}

	private boolean isLoggedIn(final String remoteUser) {
		try {
			if (remoteUser == null) {
				return false;
			}
			final String cachekey = String.join(".", CACHEKEY_USER_LOGGED_IN_CHECK, remoteUser);
			final Object obj = distributedUserContextMap.get(cachekey);

			Boolean isloggedIn = (obj == null ? false : (Boolean) obj);
			if (!isloggedIn) {
				PumaProfile profile = pumaHome.getProfile();
				isloggedIn = Objects.nonNull(profile.getCurrentUser());
				distributedUserContextMap.put(cachekey, isloggedIn, 16, -1, 28800, EntryInfo.SHARED_PUSH_PULL,
						new Object[] { isloggedIn });
			}
			return isloggedIn;
		} catch (PumaException e) {
			throw new UserException("Could not ", e);
		}
	}

	private boolean isBase64(String src) {
		try {
			Base64.getDecoder().decode(src);
			return true;
		} catch (final IllegalArgumentException e) {
			return false;
		}
	}

	/**
	 * Finds the UserDN encoded in a Temp Key. UserDN is between beginning and ending of Temp Key. Example:
	 * acp.userctx.CN=Niels Person,OU=412,O=SMC1
	 * 
	 * Where "acp.userctx." is beginning And "1" is ending.
	 * 
	 * @param tempKey
	 * @param beginning
	 * @param ending
	 * @return
	 */
	private String findUserDnFromTempKey(final String tempKey, final String beginning, final String ending) {
		if (tempKey.startsWith(beginning) && tempKey.endsWith(ending)) {
			return tempKey.substring(beginning.length(), tempKey.length() - ending.length());

		}
		return null;
	}

	/**
	 * This is used as the key for multisession for TEMP keys
	 * 
	 * @param sessionAlias
	 * @return perfixedSessionAlias
	 */
	private String prefixSessionAlias(String sessionAlias) {
		return Optional.ofNullable(sessionAlias).orElseGet(() -> CoreConstants.DEFAULT_SESSION_ALIAS);
	}

	private HttpServletRequest getHttpServletRequest(final PortletRequest request) {
		HttpServletRequest httpServletRequest = PortletUtils.createIncludedServletRequest(request);
		while (httpServletRequest instanceof HttpServletRequestWrapper) {
			HttpServletRequestWrapper httpServletRequestWrapper = (HttpServletRequestWrapper) httpServletRequest;
			httpServletRequest = (HttpServletRequest) httpServletRequestWrapper.getRequest();
		}
		return httpServletRequest;
	}

	/**
	 * 
	 * @param cookies
	 * @param userDN
	 * @param sessionAlias
	 * @param isLoggedIn
	 * @return the userContextKey that is the key to a users session data
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	private String resolveUserContextKey(Cookie[] cookies, String userDN, String sessionAlias, boolean isLoggedIn)
			throws JsonProcessingException, IOException {
		final String prefixedSessionAlias = prefixSessionAlias(sessionAlias);
		final String defaultedSessionAlias = Optional.ofNullable(sessionAlias)
				.orElseGet(() -> CoreConstants.DEFAULT_SESSION_ALIAS);

		final ObjectMapper objectMapper = new ObjectMapper();
		// First choice.. find the user context in the ACPSESSIONALIAS cookie.
		// We might not succeed.
		for (final Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase(CoreConstants.COOKIE_NAME)) {
				// We got the cookie set
				String cookieValue = cookie.getValue();
				if (isBase64(cookieValue)) {
					cookieValue = new String(Base64.getDecoder().decode(cookieValue));
				}

				final ArrayNode entries = (ArrayNode) objectMapper.readTree(cookieValue);
				// Look for the user if any entries is available
				for (final JsonNode entry : entries) {
					if (userDN.equalsIgnoreCase(entry.get("userDN").asText())) {
						// We found a user look through the sessions registered
						ArrayNode sessionEntries = (ArrayNode) entry.get("sessions");
						for (final JsonNode sessionEntry : sessionEntries) {
							final String contextStoreKeyCandidate = Optional
									.ofNullable(sessionEntry.get(defaultedSessionAlias)).map(e -> e.asText())
									.orElse(null);
							if (Objects.nonNull(contextStoreKeyCandidate) && !contextStoreKeyCandidate.isEmpty()) {
								// Remove the temporary store key since it is
								// available in the cookie from here on.
								distributedUserContextMap.invalidate(String.join("",
										CoreConstants.USER_CONTEXT_PREFIX_TEMP, userDN, prefixedSessionAlias));
								// return the context store key.
								return String.join("", CoreConstants.USER_CONTEXT_PREFIX, contextStoreKeyCandidate);
							}
						}

						final Object obj = distributedUserContextMap.get(
								String.join("", CoreConstants.USER_CONTEXT_PREFIX_TEMP, userDN, prefixedSessionAlias));
						String contextStoreKey = (obj == null ? null : (String) obj);

						if (Objects.isNull(contextStoreKey) || contextStoreKey.isEmpty()) {
							contextStoreKey = String.join(":", userDN, defaultedSessionAlias,
									UUID.randomUUID().toString());

							distributedUserContextMap.put(
									String.join("", CoreConstants.USER_CONTEXT_PREFIX_TEMP, userDN,
											prefixedSessionAlias),
									contextStoreKey, 1, -1, 120, EntryInfo.SHARED_PUSH_PULL, new Object[] { userDN });
						}

						return String.join("", CoreConstants.USER_CONTEXT_PREFIX, contextStoreKey);
					}
				}
				break;
			}
		}

		// Cookie gave no result, try temporary storage.
		final Object obj = distributedUserContextMap
				.get(String.join("", CoreConstants.USER_CONTEXT_PREFIX_TEMP, userDN, prefixedSessionAlias));
		String contextStoreKey = (obj == null ? null : (String) obj);

		if (Objects.isNull(contextStoreKey) || contextStoreKey.isEmpty()) {
			// Create it and register it if nothing has been previously
			// registered

			if (!isLoggedIn) {
				// Create an anonymous unique context key
				contextStoreKey = String.join("", CoreConstants.USER_CONTEXT_PREFIX,
						CoreConstants.USER_CONTEXT_PREFIX_UNAUTHORIZED, UUID.randomUUID().toString());

				distributedUserContextMap.put(
						String.join("", CoreConstants.USER_CONTEXT_PREFIX_TEMP, userDN, prefixedSessionAlias),
						contextStoreKey, 1, -1, 120, EntryInfo.SHARED_PUSH_PULL, new Object[] { userDN });

				return contextStoreKey;
			}

			// Create a logged in user context key
			contextStoreKey = String.join(":", userDN, defaultedSessionAlias, UUID.randomUUID().toString());

			distributedUserContextMap.put(
					String.join("", CoreConstants.USER_CONTEXT_PREFIX_TEMP, userDN, prefixedSessionAlias),
					contextStoreKey, 1, -1, 120, EntryInfo.SHARED_PUSH_PULL, new Object[] { userDN });
		}

		return contextStoreKey;
	}
}
