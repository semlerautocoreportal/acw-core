package dk.semlerit.autocore.web.core.util;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * General assertion utility meant for simple validations.<br/>
 * Can be imported static for easy access like: {@code import dk.semlerit.autocore.web.core.util.Assert.*;}
 * 
 * @author edbbrpe
 * @version 1.0
 * @since 3.0.0
 */
public class Assert {

	/**
	 * Assert a boolean expression, throwing the exception delivered through the supplier
	 * 
	 * @param expression
	 *            a boolean expression
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the expression resolves to {@literal false}
	 */
	public static <X extends Throwable> void isTrueElseThrow(boolean expression,
			Supplier<? extends X> exceptionSupplier) throws X {
		if (!expression) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert a boolean expression, throwing {@code IllegalArgumentException} if the test result is {@code false}.
	 * 
	 * <pre class="code">
	 * Assert.isTrue(i &gt; 0, &quot;The value must be greater than zero&quot;);
	 * </pre>
	 * 
	 * @param expression
	 *            a boolean expression
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if expression is {@code false}
	 */
	public static void isTrue(boolean expression, final String message) {
		isTrueElseThrow(expression, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert a boolean expression, throwing {@code IllegalArgumentException} if the test result is {@code false}.
	 * 
	 * <pre class="code">
	 * Assert.isTrue(i &gt; 0);
	 * </pre>
	 * 
	 * @param expression
	 *            a boolean expression
	 * @throws IllegalArgumentException
	 *             if expression is {@code false}
	 */
	public static void isTrue(final boolean expression) {
		isTrue(expression, "[Assertion failed] - this expression must be true");
	}

	/**
	 * Assert that the given String is not empty; that is, it must not be {@code null} and not the empty String.
	 * 
	 * <pre>
	 * Assert.hasLength(name, &quot;Name must not be empty&quot;);
	 * </pre>
	 * 
	 * @param text
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the text is null or empty
	 */
	public static <X extends Throwable> void hasLength(String text, Supplier<? extends X> exceptionSupplier) throws X {
		if (Objects.isNull(text) || text.trim().length() == 0) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that the given String is not empty; that is, it must not be {@code null} and not the empty String.
	 * 
	 * <pre class="code">
	 * Assert.hasLength(name, &quot;Name must not be empty&quot;);
	 * </pre>
	 * 
	 * @param text
	 *            the String to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @see StringUtils#hasLength
	 * @throws IllegalArgumentException
	 *             if the text is empty
	 */
	public static void hasLength(final String text, final String message) {
		hasLength(text, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that the given String is not empty; that is, it must not be {@code null} and not the empty String.
	 * 
	 * <pre class="code">
	 * Assert.hasLength(name);
	 * </pre>
	 * 
	 * @param text
	 *            the String to check
	 * @see StringUtils#hasLength
	 * @throws IllegalArgumentException
	 *             if the text is empty
	 */
	public static void hasLength(final String text) {
		hasLength(text, "[Assertion failed] - this String argument must have length; it must not be null or empty");
	}

	/**
	 * Assert that the given String has valid text content; that is, it must not be {@code null} and must contain at
	 * least one non-whitespace character.
	 * 
	 * @param text
	 *            the String to check
	 * @throws X
	 *             if the text does not contain valid text content
	 */
	public static <X extends Throwable> void hasText(String text, Supplier<? extends X> exceptionSupplier) throws X {
		if (isBlank(text)) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that the given String has valid text content; that is, it must not be {@code null} and must contain at
	 * least one non-whitespace character.
	 * 
	 * <pre class="code">
	 * Assert.hasText(name, &quot;'name' must not be empty&quot;);
	 * </pre>
	 * 
	 * @param text
	 *            the String to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @see StringUtils#hasText
	 * @throws IllegalArgumentException
	 *             if the text does not contain valid text content
	 */
	public static void hasText(final String text, final String message) {
		hasText(text, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that the given String has valid text content; that is, it must not be {@code null} and must contain at
	 * least one non-whitespace character.
	 * 
	 * <pre class="code">
	 * Assert.hasText(name, &quot;'name' must not be empty&quot;);
	 * </pre>
	 * 
	 * @param text
	 *            the String to check
	 * @see StringUtils#hasText
	 * @throws IllegalArgumentException
	 *             if the text does not contain valid text content
	 */
	public static void hasText(final String text) {
		hasText(text, "[Assertion failed] - this String argument must have text; it must not be null, empty, or blank");
	}

	/**
	 * 
	 * 
	 * @param textToSearch
	 *            the text to search
	 * @param substring
	 *            the substring to find within the text
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the text contains the substring
	 */
	public static <X extends Throwable> void doesNotContain(final String textToSearch, final String substring,
			Supplier<? extends X> exceptionSupplier) throws X {
		hasLength(textToSearch, "textToSearch must not be null or empty");
		hasLength(substring, "substring must not be null or empty");

		if (textToSearch.contains(substring)) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that the given text does not contain the given substring.
	 * 
	 * <pre class="code">
	 * Assert.doesNotContain(name, &quot;rod&quot;, &quot;Name must not contain 'rod'&quot;);
	 * </pre>
	 * 
	 * @param textToSearch
	 *            the text to search
	 * @param substring
	 *            the substring to find within the text
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the text contains the substring
	 */
	public static void doesNotContain(final String textToSearch, final String substring, final String message) {
		doesNotContain(textToSearch, substring, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that the given text does not contain the given substring.
	 * 
	 * <pre>
	 * Assert.doesNotContain(name, &quot;rod&quot;);
	 * </pre>
	 * 
	 * @param textToSearch
	 *            the text to search
	 * @param substring
	 *            the substring to find within the text
	 * @throws IllegalArgumentException
	 *             if the text contains the substring
	 */
	public static void doesNotContain(final String textToSearch, final String substring) {
		doesNotContain(textToSearch, substring,
				String.format("[Assertion failed] - the String argument [%s] must not contain the substring [%s]",
						textToSearch, substring));
	}

	/**
	 * Checks if the CharSequence contains only Unicode digits. A decimal point is not a Unicode digit and returns
	 * false.
	 * 
	 * @param cs
	 *            the charsequence to check
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the char sequence is not numeric
	 */
	public static <X extends Throwable> void isNumeric(CharSequence cs, Supplier<? extends X> exceptionSupplier)
			throws X {
		if (Objects.isNull(cs) || !cs.chars().allMatch(Character::isDigit)) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * <p>
	 * Checks if the CharSequence contains only Unicode digits. A decimal point is not a Unicode digit and returns
	 * false.
	 * </p>
	 * 
	 * @param cs
	 * @param message
	 */
	public static void isNumeric(final CharSequence cs, final String message) {
		isNumeric(cs, () -> new IllegalArgumentException(message));
	}

	/**
	 * <p>
	 * Checks if the CharSequence contains only Unicode digits. A decimal point is not a Unicode digit and returns
	 * false.
	 * </p>
	 * 
	 * @param cs
	 * @return
	 */
	public static void isNumeric(final CharSequence cs) {
		isNumeric(cs, "char sequence must be numeric");
	}

	/**
	 * Assert that an array has elements; that is, it must not be {@code null} and must have at least one element.
	 * 
	 * @param array
	 *            the array to check
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the object array is {@code null} or has no elements
	 */
	public static <X extends Throwable> void notEmpty(Object[] array, Supplier<? extends X> exceptionSupplier)
			throws X {
		if (isEmpty(array)) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that an array has elements; that is, it must not be {@code null} and must have at least one element.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(array, &quot;The array must have elements&quot;);
	 * </pre>
	 * 
	 * @param array
	 *            the array to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the object array is {@code null} or has no elements
	 */
	public static void notEmpty(Object[] array, final String message) {
		notEmpty(array, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that an array has elements; that is, it must not be {@code null} and must have at least one element.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(array);
	 * </pre>
	 * 
	 * @param array
	 *            the array to check
	 * @throws IllegalArgumentException
	 *             if the object array is {@code null} or has no elements
	 */
	public static void notEmpty(final Object[] array) {
		notEmpty(array, "[Assertion failed] - this array must not be empty: it must contain at least 1 element");
	}

	/**
	 * Assert that an array has no null elements. Note: Does not complain if the array is empty!
	 * 
	 * @param array
	 *            the array to check
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the object array is {@code null} or has no elements
	 */
	public static <X extends Throwable> void noNullElements(final Object[] array,
			Supplier<? extends X> exceptionSupplier) throws X {
		if (Objects.nonNull(null)) {
			for (final Object element : array) {
				if (Objects.isNull(element)) {
					throw exceptionSupplier.get();
				}
			}
		}
	}

	/**
	 * Assert that an array has no null elements. Note: Does not complain if the array is empty!
	 * 
	 * <pre>
	 * Assert.noNullElements(array, &quot;The array must have non-null elements&quot;);
	 * </pre>
	 * 
	 * @param array
	 *            the array to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the object array contains a {@code null} element
	 */
	public static void noNullElements(Object[] array, String message) {
		noNullElements(array, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that an array has no null elements. Note: Does not complain if the array is empty!
	 * 
	 * <pre>
	 * Assert.noNullElements(array);
	 * </pre>
	 * 
	 * @param array
	 *            the array to check
	 * @throws IllegalArgumentException
	 *             if the object array contains a {@code null} element
	 */
	public static void noNullElements(final Object[] array) {
		noNullElements(array, "[Assertion failed] - this array must not contain any null elements");
	}

	/**
	 * Assert that an array has no null elements. Note: Does not complain if the array is empty!
	 * 
	 * @param array
	 *            the array to check
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the collection is {@code null} or has no elements
	 */
	public static <X extends Throwable> void notEmpty(Collection<?> collection, Supplier<? extends X> exceptionSupplier)
			throws X {
		if (Objects.isNull(collection) || collection.isEmpty()) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that a collection has elements; that is, it must not be {@code null} and must have at least one element.
	 * 
	 * <pre>
	 * Assert.notEmpty(collection, &quot;Collection must have elements&quot;);
	 * </pre>
	 * 
	 * @param collection
	 *            the collection to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the collection is {@code null} or has no elements
	 */
	public static void notEmpty(Collection<?> collection, String message) {
		notEmpty(collection, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that a collection has elements; that is, it must not be {@code null} and must have at least one element.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(collection);
	 * </pre>
	 * 
	 * @param collection
	 *            the collection to check
	 * @throws IllegalArgumentException
	 *             if the collection is {@code null} or has no elements
	 */
	public static void notEmpty(final Collection<?> collection) {
		notEmpty(collection,
				"[Assertion failed] - this collection must not be empty: it must contain at least 1 element");
	}

	/**
	 * Assert that an iterable is not null and has elements!
	 * 
	 * @param iterable
	 *            the iterable to check
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the iterable is {@code null} or has no elements
	 */
	public static <X extends Throwable> void notEmpty(final Iterable<?> iterable,
			Supplier<? extends X> exceptionSupplier) throws X {
		if (Objects.isNull(iterable) || !iterable.iterator().hasNext()) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that an iterable has elements; that is, it must not be {@code null} and must have at least one computed
	 * element.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(iterable, &quot;Iterable must have elements&quot;);
	 * </pre>
	 * 
	 * @param iterable
	 *            the iterable to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the iterable is {@code null} or has no elements
	 */
	public static void notEmpty(final Iterable<?> iterable, final String message) {
		notEmpty(iterable, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that an iterable has elements; that is, it must not be {@code null} and must have at least one computed
	 * element.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(iterable);
	 * </pre>
	 * 
	 * @param iterable
	 *            the iterable to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the iterable is {@code null} or has no elements
	 */
	public static void notEmpty(final Iterable<?> iterable) {
		notEmpty(iterable, "[Assertion failed] - this iterable must not be empty: it must contain at least 1 element");
	}

	/**
	 * Assert that a Map has entries; that is, it must not be {@code null} and must have at least one entry.
	 * 
	 * @param map
	 *            the map to check
	 * @param exceptionSupplier
	 *            The supplier which will return the exception to be thrown
	 * @throws X
	 *             if the map is {@code null} or has no elements
	 */
	public static <X extends Throwable> void notEmpty(final Map<?, ?> map, Supplier<? extends X> exceptionSupplier)
			throws X {
		if (Objects.isNull(map) || map.isEmpty()) {
			throw exceptionSupplier.get();
		}
	}

	/**
	 * Assert that a Map has entries; that is, it must not be {@code null} and must have at least one entry.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(map, &quot;Map must have entries&quot;);
	 * </pre>
	 * 
	 * @param map
	 *            the map to check
	 * @param message
	 *            the exception message to use if the assertion fails
	 * @throws IllegalArgumentException
	 *             if the map is {@code null} or has no entries
	 */
	public static void notEmpty(final Map<?, ?> map, final String message) {
		notEmpty(map, () -> new IllegalArgumentException(message));
	}

	/**
	 * Assert that a Map has entries; that is, it must not be {@code null} and must have at least one entry.
	 * 
	 * <pre class="code">
	 * Assert.notEmpty(map);
	 * </pre>
	 * 
	 * @param map
	 *            the map to check
	 * @throws IllegalArgumentException
	 *             if the map is {@code null} or has no entries
	 */
	public static void notEmpty(final Map<?, ?> map) {
		notEmpty(map, "[Assertion failed] - this map must not be empty; it must contain at least one entry");
	}

	/**
	 * Private Constructor. No instances should be made.
	 */
	private Assert() {
	}

	/*
	 * (non-Javadoc) taken from Apache commons..
	 */
	private static boolean isBlank(final CharSequence cs) {
		if (Objects.isNull(cs) || (cs.length()) == 0 || cs.chars().allMatch(Character::isWhitespace)) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 */
	private static boolean isEmpty(Object[] array) {
		if (null == array || array.length == 0) {
			return false;
		}
		return true;
	}
}
