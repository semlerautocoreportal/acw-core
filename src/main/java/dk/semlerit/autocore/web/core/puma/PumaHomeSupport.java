/**
 * Semler It - Autocore Portal
 */
package dk.semlerit.autocore.web.core.puma;

import java.util.Optional;
import java.util.Set;

import com.ibm.portal.um.exceptions.PumaException;

/**
 * 
 *
 * @author admbrpe
 * @version 1.0
 * @since 1.0.0
 */
public interface PumaHomeSupport {

	/**
	 * 
	 * @return
	 */
	boolean isLoggedInCurrentUser();

	/**
	 * 
	 * @return
	 * @throws PumaException
	 */
	boolean isAdminCurrentUser() throws PumaException;

	/**
	 * 
	 * @return
	 * @throws PumaException
	 */
	Optional<String> getEmailCurrentUser() throws PumaException;

	/**
	 * 
	 * @param pumaHome
	 * @return
	 * @throws PumaException
	 */
	Set<String> getAccessGroupsCurrentUser() throws PumaException;

	/**
	 * 
	 * @param groupName
	 * @return
	 * @throws PumaException
	 */
	boolean isInGroupCurrentUser(String groupName) throws PumaException;

	/**
	 * 
	 * @param pumaHome
	 * @return
	 * @throws PumaException
	 */
	Optional<String> getRACFIdCurrentUser() throws PumaException;
}
