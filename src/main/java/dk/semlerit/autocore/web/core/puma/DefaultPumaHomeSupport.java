/**
 * Semler It - Autocore Portal
 */
package dk.semlerit.autocore.web.core.puma;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;

import org.apache.commons.lang3.StringUtils;

import com.ibm.portal.um.Group;
import com.ibm.portal.um.Principal;
import com.ibm.portal.um.PumaHome;
import com.ibm.portal.um.PumaLocator;
import com.ibm.portal.um.PumaProfile;
import com.ibm.portal.um.exceptions.PumaAttributeException;
import com.ibm.portal.um.exceptions.PumaException;
import com.ibm.portal.um.exceptions.PumaMissingAccessRightsException;
import com.ibm.portal.um.exceptions.PumaModelException;
import com.ibm.portal.um.exceptions.PumaSystemException;

import dk.semlerit.autocore.web.annotation.Beta;
import dk.semlerit.autocore.web.exception.UserException;
import lombok.NonNull;
import lombok.Setter;

/**
 * Default implementation of the PUMA API support interface.
 *
 * @author admbrpe
 * @version 1.0
 * @since 1.0.0
 * @see PumaHomeSupport
 */
@Beta
@ApplicationScoped
public class DefaultPumaHomeSupport implements PumaHomeSupport {
	private static final String DEFAULT_CN_VALUE_KEY = "cnMulti";
	private static final String DEFAULT_MAIL_VALUE_KEY = "mail";

	@Setter
	private String cnValueKey = DEFAULT_CN_VALUE_KEY;
	@Setter
	private String mailValueKey = DEFAULT_MAIL_VALUE_KEY;

	@Resource(lookup = PumaHome.JNDI_NAME)
	private PumaHome pumaHome;

	@Override
	public boolean isLoggedInCurrentUser() {
		try {
			PumaProfile profile = pumaHome.getProfile();
			return Objects.nonNull(profile.getCurrentUser());
		} catch (PumaException e) {
			throw new UserException("Could not ", e);
		}
	}

	@Override
	public boolean isAdminCurrentUser() throws PumaException {
		PumaProfile pumaProfile = pumaHome.getProfile();
		PumaLocator pumaLocator = pumaHome.getLocator();
		Group group = pumaLocator.findGroupByIdentifier("cn=acp.websphere.admin");
		List<Principal> users = pumaLocator.findMembersByGroup(group, false);

		return users.contains(pumaProfile.getCurrentUser());
	}

	@Override
	public Optional<String> getEmailCurrentUser() throws PumaException {
		PumaProfile pumaProfile = pumaHome.getProfile();

		return Optional.ofNullable(String.valueOf(pumaProfile
				.getAttributes(pumaProfile.getCurrentUser(), Arrays.asList(mailValueKey)).get(mailValueKey)));
	}

	@Override
	public Set<String> getAccessGroupsCurrentUser() throws PumaException {
		PumaProfile pumaProfile = pumaHome.getProfile();
		PumaLocator pumaLocator = pumaHome.getLocator();

		return pumaLocator.findGroupsByPrincipal(pumaProfile.getCurrentUser(), false).stream().map(group -> {
			try {
				Map<String, Object> attrs = pumaProfile.getAttributes(group, Arrays.asList("cn"));
				return (String) attrs.get("cn");
			} catch (PumaSystemException | PumaAttributeException | PumaModelException
					| PumaMissingAccessRightsException e) {
				throw new RuntimeException(
						String.format("Could not extract attributes for group: %s", group.getObjectID()), e);
			}
		}).filter(name -> {
			return name.startsWith("autocore.");
		}).map(name -> {
			return name.replaceAll("\\.\\d+$", "");
		}).collect(Collectors.toSet());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Optional<String> getRACFIdCurrentUser() throws PumaException {
		PumaProfile pumaProfile = pumaHome.getProfile();
		Map<String, Object> attributes = pumaProfile.getAttributes(pumaProfile.getCurrentUser(),
				Arrays.asList(cnValueKey));
		List<String> cnValues = (List<String>) attributes.getOrDefault(cnValueKey, Collections.emptyList());

		String racfId = null;
		int priority = 100;

		for (String cn : cnValues) {

			if (cn == null) {
				continue;
			}
			// INC18092: We ignore cn strings that are longer then 8 chars
			if (cn.length() > 8) {
				continue;
			}

			// Scan for pattern FXXXY match
			if (cn.length() > 4 && (cn.charAt(0) == 'F' || cn.charAt(0) == 'f')
					&& cn.substring(1, 4).chars().allMatch(Character::isDigit)) {
				// priority = 1; //NOT NEEDED because we just return the
				// racfId immediately
				return Optional.of(cn);
			}

			// Scan for pattern EDBY match
			if (priority > 2 && cn.length() > 4 && "EDB".equalsIgnoreCase(cn.substring(0, 3))) {
				racfId = cn;
				priority = 2;
				continue;
			}

			// Scan for pattern SMCY match
			if (priority > 3 && cn.length() > 4 && "SMC".equalsIgnoreCase(cn.substring(0, 3))) {
				racfId = cn;
				priority = 3;
				continue;
			}

			// Scan for pattern PRAY match
			if (priority > 4 && cn.length() > 4 && "PRA".equalsIgnoreCase(cn.substring(0, 3))) {
				racfId = cn;
				priority = 4;
				continue;
			}

			// Scan for pattern SKAY match
			if (priority > 5 && cn.length() > 4 && "SKA".equalsIgnoreCase(cn.substring(0, 3))) {
				racfId = cn;
				priority = 5;
				continue;
			}

			// Scan for pattern EXTY match
			if (priority > 6 && cn.length() > 4 && "EXT".equalsIgnoreCase(cn.substring(0, 3))) {
				racfId = cn;
				priority = 6;
				continue;
			}

			// Scan for pattern TESTUSRX match
			if (priority > 7 && cn.length() > 7 && "TESTUSR".equalsIgnoreCase(cn.substring(0, 7))) {
				racfId = cn;
				priority = 7;
				continue;
			}

			// Scan for pattern TESTX match
			if (priority > 8 && StringUtils.length(cn) > 4 && "TEST".equalsIgnoreCase(StringUtils.substring(cn, 0, 4))
					&& StringUtils.isNumeric(StringUtils.substring(cn, 4))) {
				racfId = cn;
				priority = 8;
				continue;
			}

		}

		return Optional.ofNullable(racfId);
	}

	@Override
	public boolean isInGroupCurrentUser(String groupName) throws PumaException {
		return getAccessGroupsCurrentUser().contains(groupName);
	}

	public void setPumaHome(@NonNull PumaHome pumaHome) {
		this.pumaHome = pumaHome;
	}
}
