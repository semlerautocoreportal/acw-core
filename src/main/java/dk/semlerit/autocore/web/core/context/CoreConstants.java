/**
 * Semler It - Autocore Portal
 */
package dk.semlerit.autocore.web.core.context;

import dk.semlerit.autocore.web.annotation.Beta;

/**
 * 
 *
 * @author admbrpe
 * @version 1.0
 * @since 1.0.0
 */
@Beta
public interface CoreConstants {
	/** Multiple Session constants */
	String SESSION_ALIAS_PARAM = "_S"; // This should be something that is not contained in LDAP DN (distinguished name)
	String DEFAULT_SESSION_ALIAS = "1";
	String COOKIE_NAME = "ACPSESSIONALIAS";

	/** Global user Context constants */
	String USER_CONTEXT_PREFIX_UNAUTHORIZED = "unauthenticated.user.";
	String USER_CONTEXT_JNDINAME = "services/cache/autocore/acusercontext";
	String USER_CONTEXT_PREFIX = "acp.userctx.";
	String USER_CONTEXT_PREFIX_TEMP = String.join("", USER_CONTEXT_PREFIX, "temp.");

}
